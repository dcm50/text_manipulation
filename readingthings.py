def readingthings(filename):
    with open(filename, 'r', encoding= 'utf-8') as f:
        readstuff = f.readline()
        print('Result of readline')
        print(readstuff)
        print(len(readstuff))
        stripped = readstuff.rstrip('\n')
        print(stripped)
        print(len(stripped))
        f.close()
    with open(filename, 'r', encoding= 'utf-8') as f:
        readstuffs = f.readlines()
        print('Results of readlines')
        print(readstuffs)
        print(len(readstuffs))
        f.close()

def count_men(filename):
    with open(filename, 'r', encoding= 'utf-8') as f:
        words = []
        for line in f:
            for word in line.split():
                words.append(word)
        print(words)
        count = 0
        for i in range(0, len(words)):
            if words[i] == 'men':
                count += 1
        print(count)
        f.close()

def capitilize_women(filename):
    with open(filename, 'r', encoding= 'utf-8') as f:
        words = []
        for line in f:
            for word in line.split():
                if word == 'women':
                    words.append('WOMEN')
                else:
                    words.append(word)
        f.close
    with open('example_text_new.txt', 'w', encoding= 'utf-8') as f:
        print(words)
        tempString = ''
        for i in range(0, len(words)):
            tempString= tempString + words[i]+ ' '
        f.write(tempString)
        f.close()



if __name__ == '__main__':
    readingthings('example_text.txt')
    count_men('example_text.txt')
    capitilize_women('example_text.txt')